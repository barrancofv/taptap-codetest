package com.taptapnetworks.codetest;

public class SMPP {
    private final String host;
    private final String port;

    public SMPP(String host, String port) {
        this.host = host;
        this.port = port;
    }

    public void openConnection(String userName, String password) {
        String hiddenPassword = hidePassword(password);
        System.out.println(String.format("Logged %s with password %s to %s:%s", userName, hiddenPassword, host, port));
    }

    private String hidePassword(String password) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < password.length(); i++) {
            sb.append("*");
        }
        return sb.toString();
    }

    public void send(String to, String message) {
        System.out.println(String.format("Sent message to %s", to));
        System.out.println(message);
    }

    public void closeConnection() {
        System.out.println("Connection closed");
    }

}
