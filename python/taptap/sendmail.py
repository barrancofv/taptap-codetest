class SendMailConnection:
    def __init__(self, mail_server):
        self.mail_server = mail_server
        print "Created SendMailConnection for server %s" % mail_server
        
    def prepare_message(self, to, message, subject):
        print "Preparing message for %s" % to
        print "Subject: %s" % subject
        print message
        
    def send(self):
        print 'Message sent!!!'
        
    def close(self):
        print 'Mail connection closed'