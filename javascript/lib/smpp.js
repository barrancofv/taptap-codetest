var format = require('util').format;

var Smpp = function(host, port) {
    this.host = host;
    this.port = port;
}

Smpp.prototype.open = function(user, password) {
    hidden_password = this._hide_password(password);
    console.log(format("Logged %s with password %s to %s:%s", user, hidden_password, this.host, this.port));
}

Smpp.prototype._hide_password = function(password) {
    var result = '';
    for(var i = 0; i < password.length; i++) {
        result += '*';
    }
    return result;
}

Smpp.prototype.send = function(to, message) {
    console.log(format("Sent message to %s", to));
    console.log(message);
}

Smpp.prototype.close = function() {
    console.log('Connection close');
}

module.exports = Smpp;